#! /usr/bin/env python3

from flask import Flask, render_template
app = Flask(__name__)
app.debug = True
from db import INITIAL_DATA, DB

DTodo = {'Azurency':["Design","Development","Suicide"],'Sboune':["Development","PHP"]}

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    database = DB()
    data = database.get(name)
    todo = []
    for elt in data:
        todo += [elt[1]]
    return render_template(
        "user.html",
        name=name,
        todo=todo)

@app.route('/users/')
def users():
    database = DB()
    data = []
    for user in database.users():
        print(user)
        data += [(user, database.get(user))]
    return render_template(
        "users.html",
        data=data)
    
      
if __name__ == '__main__':
    app.run(host="0.0.0.0")
